import { user } from '../src/app/models/user.model';
import { userpost } from '../src/app/models/userpost.model';
import { data } from '../src/app/models/data.model';
import { comments } from '../src/app/models/comments.model';
//IMPORT NEW DATAMODEL

export class NDataModel {
user: user;
userpost: userpost;
data: data;
comments: comments;
//DECLARE NEW VARIABLE

constructor() {
this.user = new user();
this.userpost = new userpost();
this.data = new data();
this.comments = new comments();
//CREATE NEW DM INSTANCE
    }
}