/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { Router, ActivatedRoute } from '@angular/router';
import { mailserviceService } from '../../services/mailservice/mailservice.service';
import { MatSort, MatTableDataSource, MatPaginator } from '@angular/material';
import { userserviceService } from '../../services/userservice/userservice.service';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, } from '@angular/material';
//import { showshareComponent } from '../../components/showshareComponent/showshare.component';
import { Observable } from 'rxjs';




@Component({
    selector: 'bh-viewpage',
    templateUrl: './viewpage.template.html'
})



export class viewpageComponent extends NBaseComponent implements OnInit {

    displayedColumns: string[] = ['title', 'body', 'author', 'createddate', 'Action', 'comments', 'share', 'edit', 'delete'];
    dataSource: any = [];
    // dataSource = new MatTableDataSource(this.showView)


    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;


    mm: ModelMethods;
    //data: any = [];
    isManager: boolean = true;
    likeViewName: any = [];
    UserName: any = [];
    showView: any = [];
    actionButtonLabel: string = '';
    action: boolean = true;
    setAutoHide: boolean = true;
    autoHide: number = 1000;
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';
    verticalPosition: MatSnackBarVerticalPosition = 'bottom';

    addExtraClass: boolean = false;


    constructor(private bdms: NDataModelService, private router: Router, public snackBar: MatSnackBar, private uservice: userserviceService, private route: ActivatedRoute, private mailservice: mailserviceService) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    getitem() {

    }

    ngOnInit() {


        this.uservice.getpost().subscribe((res) => {
            console.log('user:', res);
            this.showView = res;
            console.log('getting values', this.showView);
            this.dataSource = new MatTableDataSource(this.showView);
                this.dataSource.paginator = this.showView.length;
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;

        });
        console.log('getting values11', this.showView)

        if (JSON.parse(sessionStorage.userObj).groupList[0] !== 'Manager')
            this.isManager = false;




        // this.get('userpost', { flag: true });


        this.UserName = sessionStorage.userObj;
        this.likeViewName = JSON.parse(this.UserName).firstName;


    }


    // ngOnChanges() {
    //     this.get('userpost', { flag: true });
    // }

    userlikes(element) {
        var likeName;
        this.dm.data.likes = this.likeViewName;
        var likesFlag = false;
        var test = element.likes.filter(likeName => {
            if (likeName.likes == this.likeViewName)
                likesFlag = true;
        });


        if (likesFlag == false) {
            this.put('data', this.dm.data);

            this.dm.userpost = element;
            this.dm.userpost.likes.push(this.dm.data);

            this.updateById('userpost', element._id, this.dm.userpost)
        }
        //checking likes length
        if (element.likes.length == 0) {
            this.dm.data.likes = this.likeViewName;
            this.put('data', this.dm.data);

            this.dm.userpost = element;
            this.dm.userpost.likes.push(this.dm.data);

            this.updateById('userpost', element._id, this.dm.userpost)
            this.get('userpost', { flag: true });
        }
    }

    UpdateEvent(value) {
        this.uservice.UserData(value);
    }

    deleteEvent(value) {
        console.log('delete user', this.showView);
        var deleteName;
        //this.dm.data.likes = this.likeViewName;
        //this.dm.userpost.flag = false;
        var test = this.showView.filter(deleteName => {
            console.log('aa', deleteName.flag);
            if (deleteName.flag == true) {
                this.dm.userpost = value;

                this.dm.userpost.flag = false;
                console.log('bb', this.dm.userpost);

                this.updateById('userpost', value._id, this.dm.userpost)

                console.log('content', this.showView);
                this.showView.splice(value.title, 1);
                console.log('content removed', this.showView);

                console.log('aa', this.dm.userpost);
                this.get('userpost', { flag: true });

            }
        });

        // this.deleteById('userpost', _id);
        this.get('userpost', { flag: true });


    }


    mail(value) {
        this.getById('userpost', value._id);
        this.dm.userpost = value;
        let config = new MatSnackBarConfig();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0;
        // config.extraClasses = this.addExtraClass ? ['test'] : undefined;
        this.snackBar.open('Shared Via Gmail....', this.action ? this.actionButtonLabel : '', config);
        this.mailservice.sendEmail(this.dm.userpost);
    }


    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    get(dataModelName, filter?, keys?, sort?, pagenumber?, pagesize?) {
        this.mm.get(dataModelName, this, filter, keys, sort, pagenumber, pagesize,
            result => {
                //this.showView = result;
                


                // On Success code here
            },
            error => {
                // Handle errors here
            });
    }

    getById(dataModelName, dataModelId) {
        this.mm.getById(dataModelName, dataModelId,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            })
    }

    put(dataModelName, dataModelObject) {
        this.mm.put(dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    validatePut(formObj, dataModelName, dataModelObject) {
        this.mm.validatePut(formObj, dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    update(dataModelName, update, filter, options) {
        const updateObject = {
            update: update,
            filter: filter,
            options: options
        };
        this.mm.update(dataModelName, updateObject,
            result => {
                //  On Success code here
            }, error => {
                // Handle errors here
            })
    }

    delete(dataModelName, filter) {
        this.mm.delete(dataModelName, filter,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    deleteById(dataModelName, dataModelId) {
        this.mm.deleteById(dataModelName, dataModelId,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    updateById(dataModelName, dataModelId, dataModelObj) {
        this.mm.updateById(dataModelName, dataModelId, dataModelObj,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }


}
