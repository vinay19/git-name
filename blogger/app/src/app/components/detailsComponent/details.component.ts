/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { Router, ActivatedRoute } from '@angular/router';
import { userserviceService } from '../../services/userservice/userservice.service';



/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */



@Component({
    selector: 'bh-details',
    templateUrl: './details.template.html'
})
export class detailsComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    commentView: any = [];
    isManager:boolean = true;
    comments: any = [];
    postName: any = [];
    name: object;
    postViewName: any = [];
    userComment: any = [];
    user: any = [];
    commentarr:any =[];
    userValueComment: any = [];    
    viewuser = { title: '', body: '', author: '', createddate: new Date(), comment: [], likes: [],flag:'' };


    constructor(private bdms: NDataModelService, private uservice: userserviceService, private router: Router, private route: ActivatedRoute) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        var uservalue;
        var updateValue = this.uservice.uservalue;
        this.getById('userpost', updateValue._id);
        this.get('userpost',{flag:true});
        if (JSON.parse(sessionStorage.userObj).groupList[0] !== 'Manager')
            this.isManager = false;



        this.postName = sessionStorage.userObj;
        this.postViewName = JSON.parse(this.postName).firstName;
        this.name = this.postViewName;
      
    }

    updateview(value) {
        this.uservice.EditUser(value);
    }

    AddComments(value) {
        this.dm.userpost.title = this.user.title;
        this.dm.userpost.body = this.user.body;
        this.dm.userpost.author = this.user.author;
        this.dm.userpost.createddate = this.user.createddate;
        this.dm.userpost.likes = this.user.likes;
        this.dm.userpost.flag = this.user.flag;
        var objname = JSON.stringify({ commentor: this.postViewName, comments: this.dm.comments.comments });
       

        this.user.comments.push(objname);

        this.dm.comments.comments = this.user.comments;
        this.put('comments',this.dm.comments);
        // for(let i of this.user){
        //     this.userComments = JSON.parse(this.user[i]);
        // }
                //        
        

        this.dm.userpost.comments = value.comments;
        this.updateById('userpost', value._id, this.dm.userpost);


       this.dm.comments.comments = null;
        this.getById('userpost',value._id);


       
    }

    get(dataModelName, filter?, keys?, sort?, pagenumber?, pagesize?) {
        this.mm.get(dataModelName, this, filter, keys, sort, pagenumber, pagesize,
            result => {
                this.commentView = result;
                // On Success code here
            },
            error => {
                // Handle errors here
            });
    }

    getById(dataModelName, dataModelId) {
        this.mm.getById(dataModelName, dataModelId,
            result => {
                this.user=result.result;
                this.userValueComment=this.user.comments;
                this.commentarr = [];
                for(let i=0;i<this.userValueComment.length;i++){
                    this.commentarr.push(JSON.parse(this.userValueComment[i]));
                }
            
            },
            error => {
                // Handle errors here
            })
    }

    put(dataModelName, dataModelObject) {
        this.mm.put(dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    validatePut(formObj, dataModelName, dataModelObject) {
        this.mm.validatePut(formObj, dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    update(dataModelName, update, filter, options) {
        const updateObject = {
            update: update,
            filter: filter,
            options: options
        };
        this.mm.update(dataModelName, updateObject,
            result => {
                //  On Success code here
            }, error => {
                // Handle errors here
            })
    }

    delete(dataModelName, filter) {
        this.mm.delete(dataModelName, filter,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    deleteById(dataModelName, dataModelId) {
        this.mm.deleteById(dataModelName, dataModelId,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    updateById(dataModelName, dataModelId, dataModelObj) {
        this.mm.updateById(dataModelName, dataModelId, dataModelObj,
            result => {
                this.viewuser = result;


                // On Success code here
            }, error => {
                // Handle errors here
            })
    }


}
