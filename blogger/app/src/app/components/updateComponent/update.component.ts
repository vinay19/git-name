/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { Router, ActivatedRoute } from '@angular/router';
import { userserviceService } from '../../services/userservice/userservice.service';


/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-update',
    templateUrl: './update.template.html'
})

export class updateComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    user: any = [];
    viewModel: any = [];
    viewData = { title: '', body: '', author: '', createddate: new Date(), comments: [], likes: [],flag:true};

    constructor(private bdms: NDataModelService, private router: Router, private uservice: userserviceService, private route: ActivatedRoute) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        var userupdate;
        var Updatedvalue = this.uservice.userupdate;
        this.getById('userpost', Updatedvalue._id);
        this.get('userpost',{flag:true});
    }

    updateview() {
        var userupdate;
        var Updatedvalue = this.uservice.userupdate;


        this.updateById('userpost', Updatedvalue._id, this.dm.userpost);
        this.router.navigate(['/home/viewpage']);

    }

    get(dataModelName, filter?, keys?, sort?, pagenumber?, pagesize?) {
        this.mm.get(dataModelName, this, filter, keys, sort, pagenumber, pagesize,
            result => {
                this.viewModel = result;
                // On Success code here
            },
            error => {
                // Handle errors here
            });
    }

    getById(dataModelName, dataModelId) {
        this.mm.getById(dataModelName, dataModelId,
            result => {
                this.viewData = result.result;
                this.dm.userpost.title = this.viewData.title;
                this.dm.userpost.body = this.viewData.body;
                this.dm.userpost.author = this.viewData.author;
                this.dm.userpost.createddate = this.viewData.createddate;
                this.dm.userpost.comments = this.viewData.comments;
                this.dm.userpost.likes = this.viewData.likes;
                this.dm.userpost.flag = this.viewData.flag;
            },
            error => {
                // Handle errors here
            })
    }

    put(dataModelName, dataModelObject) {
        this.mm.put(dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    validatePut(formObj, dataModelName, dataModelObject) {
        this.mm.validatePut(formObj, dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    update(dataModelName, update, filter, options) {
        const updateObject = {
            update: update,
            filter: filter,
            options: options
        };
        this.mm.update(dataModelName, updateObject,
            result => {
                //  On Success code here
            }, error => {
                // Handle errors here
            })
    }

    delete(dataModelName, filter) {
        this.mm.delete(dataModelName, filter,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    deleteById(dataModelName, dataModelId) {
        this.mm.deleteById(dataModelName, dataModelId,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    updateById(dataModelName, dataModelId, dataModelObj) {
        this.mm.updateById(dataModelName, dataModelId, dataModelObj,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }


}
