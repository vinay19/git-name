/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-dashboard',
    templateUrl: './dashboard.template.html'
})

export class dashboardComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    showview: any = [];
    updateview: any = [];
    isManager: boolean = false;
    count: number = 0;
    postView: any = [];
    total: number;
    likesCount: number;
    likesValue;
    viewData: any = [];
    likeViewName: any = [];
    likeName: any = [];
    lName: object;
    likeUserValue: number = 0;
    nameValue: any = [];
    countlikes: number = 0;
    val: any = [];
    bloggerName;


    constructor(private bdms: NDataModelService) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {

        if (JSON.parse(sessionStorage.userObj).groupList[0] == 'Manager')
            this.isManager = true;
        this.get('userpost', { flag: true });
        this.bloggerName = (JSON.parse(sessionStorage.userObj).groupList[0]);

    }
    postViewData() {
        let sum = 0;
        for (let i of this.viewData) {
            sum = sum + i.comments.length;
        }
        return sum;
    }
    likesdata() {
        let sum = 0;
        for (let i of this.viewData) {
            sum = sum + i.likes.length;
        }
        return sum;
    }

    likesuser() {
        let value = [];


        this.likeName = sessionStorage.userObj;
        this.likeViewName = JSON.parse(this.likeName).firstName;
        this.lName = this.likeViewName;

        for (let i of this.viewData) {

            for (let j of i.likes) {
                if (this.lName == j.likes) {
                    this.likeUserValue = this.likeUserValue + 1;
                    this.nameValue = this.likeUserValue;
                }

            }
        }
        return this.nameValue;
    }

    commentuser() {
        let value = [];


        this.likeName = sessionStorage.userObj;
        this.likeViewName = JSON.parse(this.likeName).firstName;
        this.lName = this.likeViewName;

        for (let i of this.viewData) {
            for (let j of i.comments) {
                if (this.lName == JSON.parse(j).commentor) {
                    this.countlikes = this.countlikes + 1;
                    this.val = this.countlikes;
                }

            }
        }
        return this.val;





    }


    get(dataModelName, filter?, keys?, sort?, pagenumber?, pagesize?) {
        this.mm.get(dataModelName, this, filter, keys, sort, pagenumber, pagesize,
            result => {
                this.viewData = result;


                if (this.bloggerName == 'Manager') {
                    this.likesCount = this.likesdata();
                    this.total = this.postViewData();
                }
                if (this.bloggerName !== 'Manager') {
                    this.likesCount = this.likesuser();
                    this.total = this.commentuser();
                }
                this.postView.push(this.viewData);

                // On Success code here
            },
            error => {
                // Handle errors here
            });
    }

    getById(dataModelName, dataModelId) {
        this.mm.getById(dataModelName, dataModelId,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            })
    }

    put(dataModelName, dataModelObject) {
        this.mm.put(dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    validatePut(formObj, dataModelName, dataModelObject) {
        this.mm.validatePut(formObj, dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    update(dataModelName, update, filter, options) {
        const updateObject = {
            update: update,
            filter: filter,
            options: options
        };
        this.mm.update(dataModelName, updateObject,
            result => {
                //  On Success code here
            }, error => {
                // Handle errors here
            })
    }

    delete(dataModelName, filter) {
        this.mm.delete(dataModelName, filter,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    deleteById(dataModelName, dataModelId) {
        this.mm.deleteById(dataModelName, dataModelId,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    updateById(dataModelName, dataModelId, dataModelObj) {
        this.mm.updateById(dataModelName, dataModelId, dataModelObj,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }


}
