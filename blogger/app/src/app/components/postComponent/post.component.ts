/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { ModelMethods } from '../../lib/model.methods';
// import { BDataModelService } from '../service/bDataModel.service';
import { NDataModelService } from 'neutrinos-seed-services';
import { NBaseComponent } from '../../../../../app/baseClasses/nBase.component';
import { Router } from '@angular/router';
import { userserviceService } from '../../services/userservice/userservice.service';

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-post',
    templateUrl: './post.template.html'
})

export class postComponent extends NBaseComponent implements OnInit {
    mm: ModelMethods;
    showPost: any = [];
    viewPost: any = [];
    likeViewName: any = [];
    likeName: any = [];

    constructor(private bdms: NDataModelService, private router: Router, private uservice:userserviceService) {
        super();
        this.mm = new ModelMethods(bdms);
    }

    ngOnInit() {
        this.get('userpost', { flag: true });
        this.likeName = sessionStorage.userObj;
        this.likeViewName = JSON.parse(this.likeName).firstName;


    }

    CreatePost() {
        this.dm.userpost.author = this.likeViewName;
        this.dm.userpost.createddate = new Date();
        this.dm.userpost.flag = true;
        console.log('aa',this.dm.userpost);
        this.uservice.Createpost(this.dm.userpost);


        
        // this.put('userpost', this.dm.userpost);
        // this.get('userpost');

        this.router.navigate(['/home/viewpage']);

        this.get('userpost');




    }

    get(dataModelName, filter?, keys?, sort?, pagenumber?, pagesize?) {
        this.mm.get(dataModelName, this, filter, keys, sort, pagenumber, pagesize,
            result => {
                this.showPost = result;
                this.viewPost = [];
                for (let i = 0; i < this.showPost.length; i++) {
                    this.viewPost = this.showPost[i].createddate;
                }

                // On Success code here
            },
            error => {
                // Handle errors here
            });
    }

    getById(dataModelName, dataModelId) {
        this.mm.getById(dataModelName, dataModelId,
            result => {
                // On Success code here
            },
            error => {
                // Handle errors here
            })
    }

    put(dataModelName, dataModelObject) {
        this.mm.put(dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    validatePut(formObj, dataModelName, dataModelObject) {
        this.mm.validatePut(formObj, dataModelName, dataModelObject,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    update(dataModelName, update, filter, options) {
        const updateObject = {
            update: update,
            filter: filter,
            options: options
        };
        this.mm.update(dataModelName, updateObject,
            result => {
                //  On Success code here
            }, error => {
                // Handle errors here
            })
    }

    delete(dataModelName, filter) {
        this.mm.delete(dataModelName, filter,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    deleteById(dataModelName, dataModelId) {
        this.mm.deleteById(dataModelName, dataModelId,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }

    updateById(dataModelName, dataModelId, dataModelObj) {
        this.mm.updateById(dataModelName, dataModelId, dataModelObj,
            result => {
                // On Success code here
            }, error => {
                // Handle errors here
            })
    }


}
