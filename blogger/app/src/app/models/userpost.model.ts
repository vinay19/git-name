import {JsonProperty, JsonObject} from '../lib/tj.deserializer'
import { data } from './data.model';

@JsonObject
export class userpost {
  @JsonProperty('title', String, true)
  public title: string = undefined;

  @JsonProperty('body', String, true)
  public body: string = undefined;

  @JsonProperty('author', String, true)
  public author: string = undefined;

  @JsonProperty('createddate', Date, true)
  public createddate: Date = undefined;

  @JsonProperty('comments', [String], true)
  public comments: string[] = [];

  @JsonProperty('likes', [data], true)
  public likes: data[] = [];

  @JsonProperty('flag', Boolean, true)
  public flag: boolean = undefined;

}