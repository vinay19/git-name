import {JsonProperty, JsonObject} from '../lib/tj.deserializer'

@JsonObject
export class comments {
  @JsonProperty('comments', [String], true)
  public comments: string[] = [];

}