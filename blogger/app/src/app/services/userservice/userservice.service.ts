/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE CLASS NAME*/
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';





@Injectable()
export class userserviceService {
    constructor(private router: Router, private http: HttpClient) {

    }
    uservalue: any = [];

    UserData(value) {
        this.uservalue = value;
        console.log('data', this.uservalue);
        this.router.navigate(['/home/details']);

        // return this.uservalue;

    }

    userupdate: any = [];

    EditUser(value) {
        this.userupdate = value;
        console.log('data', this.userupdate);
        this.router.navigate(['/home/update']);

        // return this.uservalue;

    }

    Createpost(postvalue) {
        console.log('servicepost', postvalue)
        let accessToken = sessionStorage.accessToken;
        console.log(accessToken);
        let headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Auhtorization': 'Bearer' + accessToken
            }
            )
        };
        // headers.append("Authorization", "Bearer " + accessToken);

        // console.log('ss',this.http.post('http://127.0.0.1:24483/api/createpost',postvalue).subscribe((Response)));
        return this.http.put('http://127.0.0.1:24483/api/createpost', JSON.stringify(postvalue), headers).subscribe((res) => {
            console.log('user:', res);

        });
    }

    getpost() {
        let accessToken = sessionStorage.accessToken;
        console.log(accessToken);
        let headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Auhtorization': 'Bearer' + accessToken
            }
            )
        };
        return this.http.get('http://127.0.0.1:24483/api/createpost', headers);

    }
}