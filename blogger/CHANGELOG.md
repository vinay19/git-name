##3.0.1
    - manually migrated to 4.0.3 from 4.0.2

##3.0.0
    - manually migrated to 4.0.2 from 4.0.1

##2.0.0
    - manually migrated to 4.0.1 from 4.0.0-beta_3
    - changed AutohideSplashScreen Preference to false
